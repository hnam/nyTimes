import { Injectable }     from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import {Observable} from 'rxjs/Rx';

declare const $: any;

@Injectable()
export class ApiService {
    constructor (public http: Http) {
    }
    get(api: string, params: {}) {
        if (!api || api.length === 0) {
            return ;
        }
        let qStr = $.param(params);
        return this.http.get( api + "?" + qStr)
            .map($.proxy(this.extractData, this))
            .catch(this.handleError);
    }
    public extractData(res: Response) {
        let body = res.json();
        return body || {};
    }
    public handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || "";
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ""} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.log(errMsg);

        return Observable.throw(errMsg);
    }
}
