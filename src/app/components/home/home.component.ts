import { Component } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ApiService } from '../../services/index';

declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent{
	public is_load: boolean = false;
	public data: any = [];
	public currentPage: number = 1;
	public api_key: any = '5763846de30d489aa867f0711e2b031c';
	public q:any = 'singapore';
	public params: any;
	public apiRoot: string = "https://api.nytimes.com/svc/search/v2/articlesearch.json";

	constructor(private _apiService: ApiService,public ngxSmartModalService: NgxSmartModalService) {
		this.loadData(this.currentPage);
    }
	loadData(pagi){
		this.is_load = true;
		if(pagi < 1){pagi = 1; return;}
		this.params = { api_key:this.api_key, q: this.q, page:pagi };
		this.data = [];
		if(this.is_load){
			this._apiService.get(this.apiRoot,this.params).subscribe(
				result => {
		          this.data = result.response.docs;
		          console.log(this.data)
		          this.is_load = false;
		          this.currentPage = pagi;
		        },
		        error => {
		           this.is_load = false;
		           return false;
		        }
			);
		}
	}
	page(pagination){
		this.scroll_Top();
		this.loadData(pagination);
	}
	scroll_Top() {
	    $(document).ready(function () {
	      $(this).scrollTop(0);
	    });
	}
	detail(multimedia,snippet,pub_date,source){
		let obj = {multimedia:multimedia,snippet:snippet,pub_date:pub_date,source:source};
		this.ngxSmartModalService.setModalData(obj, 'modalData', true);
		this.ngxSmartModalService.getModal('modalData').open();
	}
}







